package demo;

public class StaticDemo {

	public static void main(String[] args) {
		Loo o1 = new Loo();
		o1.show();
		Loo o2 = new Loo();
		o2.show();
		
		System.out.println();
		System.out.println(Loo.b);//建議通過 類名. 訪問
		System.out.println(o2.b);
		
		Noo o3 = new Noo();
		Noo o4 = new Noo();
	}

}

//靜態塊
class Noo{
	static {
		System.out.println("靜態塊");
	}
	Noo(){
		System.out.println("constructor");
	}
}

//靜態方法
class Moo{
	int a; //對象. 訪問
	static int b;//類名. 訪問
	void show() { //有this
		System.out.println(a);
		System.out.println(b);
	}
	static void test() { //沒有this => 沒有對象。因為this就代表對象
//		System.out.println(a); //
		System.out.println(b);
	}
}


//靜態變量
class Loo{
	int a; //實例變量
	static int b; //靜態變量
	Loo(){
		a++;
		b++;
	}
	void show() {
		System.out.println("a : "+a);
		System.out.println("b : "+b);
	}
}